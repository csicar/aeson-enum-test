module Main where

import Prelude

import Data.Generic.Rep (class Generic)
import Effect (Effect)
import Effect.Console (log)
import Foreign.Class (class Encode)
import Foreign.Generic (defaultOptions, encodeJSON, genericEncode)
import Prim (Array, String)


main :: Effect Unit
main = do
  log $ encodeJSON A

data T = A | B | C

derive instance genericT :: Generic T _


instance encodeDay :: Encode T where
  encode = genericEncode $ defaultOptions { unwrapSingleConstructors = false }