{-# LANGUAGE DeriveGeneric #-}
module Main where

import Lib
import GHC.Generics
import Data.Aeson (encode, ToJSON)

main :: IO ()
main = putStrLn $ show $ encode A


data T = A | B | C | D deriving (Generic)

instance ToJSON T